require 'spec_helper'

describe Braintree::CommandHandlers::Base do
  describe '#handle' do
    it 'raises an error' do
      expect { Braintree::CommandHandlers::Base.new('tom', []).handle }.to raise_error
    end
  end
end
