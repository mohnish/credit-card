require 'spec_helper'

describe Braintree::CommandHandlers::Charge do
  describe '#handle' do
    before do
      Braintree::User.class_variable_set :@@users, []
      Braintree::CommandHandlers::AddCard.new('tom', ['4111111111111111', '$1000']).handle
    end

    it 'charges the given card' do
      Braintree::CommandHandlers::Charge.new('tom', ['$300']).handle
      user = Braintree::User.all.last
      expect(user.name).to eql('tom')
      expect(user.default_card.balance).to eql(300)
    end
  end
end
