require 'spec_helper'

describe Braintree::CommandHandlers::Credit do
  describe '#handle' do
    before do
      Braintree::User.class_variable_set :@@users, []
      Braintree::CommandHandlers::AddCard.new('tom', ['4111111111111111', '$1000']).handle
      user = Braintree::User.find('tom')
      user.default_card.charge('$500')
    end

    it 'charges the given card' do
      Braintree::CommandHandlers::Credit.new('tom', ['$300']).handle
      user = Braintree::User.find('tom')
      expect(user.name).to eql('tom')
      expect(user.default_card.balance).to eql(200)
    end
  end
end
