require 'spec_helper'

describe Braintree::CommandHandlers::AddCard do
  describe '#handle' do
    before do
      Braintree::User.class_variable_set :@@users, []
    end

    it 'creates a user and add a card to the created user' do
      Braintree::CommandHandlers::AddCard.new('tom', ['4111111111111111', '$1000']).handle
      user = Braintree::User.all.last
      expect(user.name).to eql('tom')
      expect(user.default_card.card_number).to eql('4111111111111111')
    end
  end
end
