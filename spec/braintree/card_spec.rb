require 'spec_helper'

describe Braintree::Card do
  describe '.new' do
    it 'raises an error' do
      expect { Braintree::Card.new(card_number: '1231231', limit: '$100') }.to raise_error
    end
  end
end
