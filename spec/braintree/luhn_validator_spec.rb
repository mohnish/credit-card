require 'spec_helper'

describe Braintree::LuhnValidator do
  before do
    Braintree::User.class_variable_set :@@users, []
  end

  describe '.valid?' do
    VALID_CARDS = ['4111111111111111', '4024007196669963', '5385886811922497', '6011830290824113', '347283591433010', '123451234512348']
    INVALID_CARDS = ['123', '23994700000053866078', '21312312']

    context 'when the card is valid' do
      it 'returns true' do
        VALID_CARDS.each do |valid_card|
          expect(Braintree::LuhnValidator.valid?(valid_card)).to be_truthy
        end
      end
    end

    context 'when the card is invalid' do
      it 'returns false' do
        INVALID_CARDS.each do |invalid_card|
          expect(Braintree::LuhnValidator.valid?(invalid_card)).to be_falsey
        end
      end
    end
  end
end
