require 'spec_helper'

describe Braintree::CreditCard do
  describe '#charge' do
    context 'when the charged amount keeps the balance within the limit' do
      let(:card) { Braintree::CreditCard.new(card_number: '4111111111111111', limit: '$1000') }

      it 'charges the card' do
        card.charge('$900')
        expect(card.balance).to eql(900)
      end
    end

    context 'when the charged amount makes the balance over the limit' do
      let(:card) { Braintree::CreditCard.new(card_number: '4111111111111111', limit: '$1000') }

      it 'declines the charge' do
        card.charge('$2000')
        expect(card.balance).to eql(0)
      end
    end
  end

  describe '#credit' do
    context 'when the balance is $0' do
      let(:card) { Braintree::CreditCard.new(card_number: '4111111111111111', limit: '$1000') }

      before do
        card.credit('$100')
      end

      it 'credits the balance to a negative amount' do
        expect(card.balance).to eql(-100)
      end
    end

    context 'when the balance is more than $0' do
      let(:card) { Braintree::CreditCard.new(card_number: '4111111111111111', limit: '$1000') }

      before do
        card.charge('$100')
        card.credit('$100')
      end

      it 'drops the balance to the credited amount' do
        expect(card.balance).to eql(0)
      end
    end

    context 'when the balance is less than $0' do
      let(:card) { Braintree::CreditCard.new(card_number: '4111111111111111', limit: '$1000') }

      before do
        card.credit('$100')
        card.credit('$100')
      end

      it 'credits the balance and leaves the balance in a negative amount' do
        expect(card.balance).to eql(-200)
      end
    end
  end
end
