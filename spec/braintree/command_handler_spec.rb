require 'spec_helper'

describe Braintree::CommandHandler do
  subject { Braintree::CommandHandler }

  describe '.handle' do
    before do
      Braintree::User.class_variable_set :@@users, []
    end

    context 'when the command is to add a card' do
      let(:command) { "Add Tom 4111111111111111 $1000" }

      before do
        subject.handle(command)
      end

      it 'creates a user and adds a card to the user' do
        users = Braintree::User.all
        expect(users.size).to eql(1)
        expect(users.first.default_card.card_number).to eql('4111111111111111')
      end
    end

    context 'when the command is to charge a card' do
      let(:command) { "Charge Tom $500" }

      before do
        subject.handle("Add Tom 4111111111111111 $1000")
        subject.handle(command)
      end

      it 'charges the given card' do
        users = Braintree::User.all
        card = users.first.default_card
        expect(users.size).to eql(1)
        expect(card.balance).to eql(500)
      end
    end

    context 'when the command is to credit a card' do
      let(:command) { "Credit Tom $100" }

      before do
        subject.handle("Add Tom 4111111111111111 $1000")
        subject.handle(command)
      end

      it 'credits the given card' do
        users = Braintree::User.all
        card = users.first.default_card
        expect(users.size).to eql(1)
        expect(card.balance).to eql(-100)
      end
    end
  end
end
