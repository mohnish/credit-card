require 'spec_helper'

describe Braintree::User do
  before do
    Braintree::User.class_variable_set :@@users, []
  end

  describe '.find' do
    context 'when the user is found' do
      before do
        Braintree::User.new(name: 'meghan')
      end

      it 'returns the user' do
        expect(Braintree::User.find('meghan')).not_to be_nil
      end
    end

    context 'when no user is found' do
      it 'returns nil' do
        expect(Braintree::User.find('mt')).to be_nil
      end
    end
  end

  describe '.all' do
    before do
      Braintree::User.new(name: 'troy')
      Braintree::User.new(name: 'sindhu')
      Braintree::User.new(name: 'meghan')
    end

    context 'when sorted flag is passed' do
      it 'returns a sorted list of users' do
        sorted_user_list = Braintree::User.all(true)
        expect(sorted_user_list.size).to eql(3)
        expect(sorted_user_list.map(&:name)).to eql(['meghan', 'sindhu', 'troy'])
      end
    end

    context 'original order is requested' do
      it 'returns the list of users' do
        assorted_user_list = Braintree::User.all
        expect(assorted_user_list.size).to eql(3)
        expect(assorted_user_list.map(&:name)).to eql(['troy', 'sindhu', 'meghan'])
      end
    end
  end

  describe '#add_card' do
    let(:user) { Braintree::User.new(name: 'sindhu') }

    it 'adds a card' do
      user.add_card(card_number: '41111111111111', limit: '$1000')
      expect(user.cards.size).to eql(1)
    end
  end

  describe '#default_card' do
    let(:user) { Braintree::User.new(name: 'meghan') }

    it 'returns the default card' do
      user.add_card(card_number: '41111111111111', limit: '$1000')
      user.add_card(card_number: '42222222222222', limit: '$1000')
      expect(user.default_card.card_number).to eql('42222222222222')
    end
  end

  describe '#summary' do
    let(:default_card) { double('card') }
    let(:user) do
      user = Braintree::User.new(name: 'troy')
      allow(user).to receive_messages(:default_card => default_card)
      user
    end

    context 'when the card is valid' do
      before do
        allow(default_card).to receive_messages(:valid? => true, :balance => 10)
      end

      it 'returns the balance' do
        expect(user.summary).to eql('troy: $10')
      end
    end

    context 'when the card is invalid' do
      before do
        allow(default_card).to receive_messages(:valid? => false)
      end

      it 'returns error' do
        expect(user.summary).to eql('troy: error')
      end
    end
  end
end
