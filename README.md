# Braintree

Coding challenge to build a credit card processor

## Installation

- Go to gem root and run `gem install braintree-0.0.2.gem`

## Usage

- Run `braintree` and the following is printed

```bash
Usage: braintree OPTIONS

Specific options:
    -i, --interactive                Start an interactive command session. type `q` to stop
    -p, --path commands.txt          Specify the path to the command file

Common options:
    -v, --version                    Print the version
    -h, --help                       Show this message
```

- Running `braintree -i` puts the program in interactive mode where the user can enter the commands

```bash
Enter the next command or 'q' to summarize & quit:
>
Add Tom 4111111111111111 $1000
Enter the next command or 'q' to summarize & quit:
>
Charge Tom $400
Enter the next command or 'q' to summarize & quit:
>
q
Tom: $400
```

- Running `braintree --path commands.txt` puts the program in 'read from file' mode

```bash
Lisa: $-93
Quincy: error
Tom: $500

```

## Setup

- Make sure you have the latest version of Ruby installed
- From the gem root, run `./bin/setup`

## Development

- To run the specs, run `rake`
- To run the program from command line, run `ruby -Ilib ./exe/braintree` from the gem root (since the gem is not built). The following usage instructions are printed:

```bash
Usage: braintree OPTIONS

Specific options:
    -i, --interactive                Start an interactive command session. type `q` to stop
    -p, --path commands.txt          Specify the path to the command file

Common options:
    -v, --version                    Print the version
    -h, --help                       Show this message
```

- Running `ruby -Ilib ./exe/braintree -i` puts the program in interactive mode where the user can enter the commands

```bash
Enter the next command or 'q' to summarize & quit:
>
Add Tom 4111111111111111 $1000
Enter the next command or 'q' to summarize & quit:
>
Charge Tom $400
Enter the next command or 'q' to summarize & quit:
>
q
Tom: $400
```
- Running `ruby -Ilib ./exe/braintree --path commands.txt` puts the program in 'read from file' mode

```bash
ruby -Ilib ./exe/braintree -p commands.txt

Lisa: $-93
Quincy: error
Tom: $500

```

## Design

- The credit card processor is implemented in Ruby. I chose Ruby because of my level of comfort with Ruby
- The program is built in a way to make it easy to support different cards or even multiple cards
- The command handlers that the program supports currently are adding a card, charging and crediting a card
- If there is a requirement to add support to a new command, we can just create a new file with the `handle`
method and add the reference to the `lib/braintree/command_handler.rb` file (to the list of `AVAILABLE_COMMANDS`)
