require 'braintree/command_handlers/base'

module Braintree
  module CommandHandlers
    class Charge < Base
      def initialize user_name, args
        super
        @amount = args.first
      end

      def handle
        @user.default_card.charge(@amount)
      end
    end
  end
end
