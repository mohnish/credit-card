require 'braintree/command_handlers/base'

module Braintree
  module CommandHandlers
    class Credit < Base
      def initialize user_name, args
        super
        @amount = args.first
      end

      def handle
        @user.default_card.credit(@amount)
      end
    end
  end
end
