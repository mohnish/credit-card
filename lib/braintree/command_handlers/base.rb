require 'braintree/user'

module Braintree
  module CommandHandlers
    class Base
      def initialize user_name, args
        @user = User.find(user_name)
      end

      def handle
        fail StandardError.new('Implement me!')
      end
    end
  end
end
