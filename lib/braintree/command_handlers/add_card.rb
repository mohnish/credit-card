require 'braintree/command_handlers/base'

module Braintree
  module CommandHandlers
    class AddCard < Base
      def initialize user_name, args
        super
        @user ||= User.new(name: user_name)
        @card_number, @limit = args
      end

      def handle
        @user.add_card(card_number: @card_number, limit: @limit)
      end
    end
  end
end
