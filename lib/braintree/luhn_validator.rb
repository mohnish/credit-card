module Braintree
  module LuhnValidator
    extend self

    def valid? number
      odd_sum = even_sum = 0

      number.reverse.chars.each_slice(2) do |odd, even|
        odd_sum += odd.to_i

        double = even.to_i * 2
        double -= 9 if double > 9
        even_sum += double
      end

      (odd_sum + even_sum) % 10 == 0
    end
  end
end
