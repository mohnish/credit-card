require 'braintree/command_handlers/add_card'
require 'braintree/command_handlers/charge'
require 'braintree/command_handlers/credit'

module Braintree
  module CommandHandler
    extend self

    AVAILABLE_COMMANDS = {
      'add' => CommandHandlers::AddCard,
      'charge' => CommandHandlers::Charge,
      'credit' => CommandHandlers::Credit
    }

    def handle command
      command_name, user_name, *command_args = command.split(' ')
      handler_class = AVAILABLE_COMMANDS[command_name.downcase]

      fail StandardError.new('Invalid command provided') if handler_class.nil?

      handler_class.new(user_name, command_args).handle
    end
  end
end
