require 'braintree/luhn_validator'

module Braintree
  class Card
    attr_accessor :card_number

    def initialize **args
      fail StandardError.new("Cannot instantiate Card class directly. Use a subtype") if self.class == Card

      @card_number = args[:card_number]
    end

    def valid?
      @valid ||= LuhnValidator.valid?(@card_number)
    end

    private
      # TODO: Extract these to a helper
      def currency_to_number currency
        currency[1..currency.size - 1].to_i
      end

      def number_to_currency number
        "$#{number}"
      end
  end
end
