require 'braintree/card'

module Braintree
  class CreditCard < Card
    attr_accessor :balance

    def initialize **args
      @limit = currency_to_number(args[:limit])
      @balance = 0
      super
    end

    def charge amount
      amount = currency_to_number(amount)
      @balance += amount if can_be_charged?(amount)
      number_to_currency(@balance)
    end

    def credit amount
      @balance -= currency_to_number(amount) if valid?
      number_to_currency(@balance)
    end

    private
      def can_be_charged? amount
        valid? && ((amount + @balance) <= @limit)
      end
  end
end
