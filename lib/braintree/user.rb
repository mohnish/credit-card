require 'braintree/credit_card'

module Braintree
  class User
    attr_accessor :name, :cards

    @@users = []

    class << self
      def find name
        @@users.select { |user| user.name.downcase == name.downcase }.last
      end

      def all(sorted = false)
        if sorted
          @@users.sort_by { |user| user.name }
        else
          @@users
        end
      end
    end

    def initialize **args
      @name = args[:name]
      @cards = []
      @@users << self
    end

    # `args` is a hash with the keys
    # - `card_number`
    # - `limit`
    def add_card **args
      credit_card = CreditCard.new(args)
      @cards << credit_card
      credit_card
    end

    def default_card
      # TODO: Since we only have 1 card currenlty, let's just
      # work with that 1 card. If we have more, we can return
      # the card with the default flag set on the card
      @cards.last
    end

    def summary
      card = default_card

      if card.valid?
        "#{@name}: $#{card.balance}"
      else
        "#{@name}: error"
      end
    end
  end
end
